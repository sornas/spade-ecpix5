from PIL import Image
f = open('generated_maps.spade', 'w')
with open('maps.txt', 'r') as sprites:
    for line in sprites:
        name = line.strip()
        im = Image.open(name)
        image = list(im.getdata())
        f.write('entity lvl_' + name[:len(name) - 4] + '(clk:clk, x: int<20>, y: int<20>) -> Tile {\n')
        f.write('    let puzzle = [\n')
        for y in range(16):
            row = '        ['
            for x in range(16):
                pixel = image[y * 16 + x]
                r = pixel[0]
                g = pixel[1]
                b = pixel[2]
                row += 'Tile::'
                if (r, g, b) == (255, 0, 0):
                    row+= 'And'
                elif (r, g, b) == (255, 0, 255):
                    row+= 'Xor'
                elif (r, g, b) == (0, 255, 0):
                    row+= 'Or'
                elif (r, g, b) == (0, 0, 255):
                    row+= 'Not'
                elif (r, g, b) == (0, 0, 0):
                    row+= 'Empty'
                elif (r, g, b) == (255, 255, 0):
                    row+= 'Num0'
                elif (r, g, b) == (0, 255, 255):
                    row+= 'Num1'
                elif (r, g, b) == (255, 255, 255):
                    row+= 'Straight0'
                elif (r, g, b) == (215, 215, 215):
                    row+= 'Straight1'
                elif (r, g, b) == (175, 175, 175):
                    row+= 'Corner0'
                elif (r, g, b) == (135, 135, 135):
                    row+= 'Corner1'
                elif (r, g, b) == (95, 95, 95):
                    row+= 'Corner2'
                elif (r, g, b) == (55, 55, 55):
                    row+= 'Corner3'
                row += '(),'
            row = row[:len(row) - 1]
            row += '],\n'
            f.write(row)
        f.write('    ];\n    reg(clk) tile: Tile = puzzle[sext(y)][sext(x)];\n    tile\n')
        f.write('}\n\n')
f.close()
