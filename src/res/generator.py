from PIL import Image
f = open('generated_sprites.spade', 'w')
with open('sprites.txt', 'r') as sprites:
    for line in sprites:
        name = line.strip()
        im = Image.open(name)
        image = list(im.getdata())
        f.write('fn sprite_' + name[:len(name) - 4] + '() -> [[int<3>; 16]; 16] {\n')

        f.write('    [\n')
        for y in range(16):
            row = '        ['
            for x in range(16):
                pixel = image[y * 16 + x]
                r = min(pixel[0], 1)
                g = min(pixel[1], 1)
                b = min(pixel[2], 1)
                row += '0b'+str(r)+str(g)+str(b)+','
            row = row[:len(row) - 1]
            row += '],\n'
            f.write(row)
        f.write('    ]\n')
        f.write('}\n\n')
f.close()
