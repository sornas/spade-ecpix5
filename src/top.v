module top
    ( input clk
    , output[2:0] ld5
    , output[2:0] ld6
    , output[2:0] ld7
    , output[2:0] ld8
    , output[7:0] pmod7
    , input[7:0] pmod3
    , input[7:0] pmod2
    , input[7:0] pmod1
    , output[7:0] pmod0
    , output[7:0] pmod4
    );

    reg rst = 1;
    always @(posedge clk) begin
        rst <= pmod2[2];
    end

    assign pmod0 = pmod2;

    assign pmod7[0] = vsync;
    assign pmod7[1] = hsync;
    assign pmod7[4] = r;
    assign pmod7[5] = g;
    assign pmod7[6] = b;

    assign btn1 = pmod2[0];
    assign btn2 = pmod2[1];
    assign btn3 = 0;

    assign dir1 = !pmod3[3];
    assign dir2 = !pmod3[4];
    assign dir3 = !pmod3[5];
    assign dir4 = !pmod3[6];
    assign dir5 = !pmod3[7];

    wire[7:0] dummy;

    e_main main
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_buttons({btn1, btn2, btn3, dir1, dir2, dir3, dir4, dir5})
        , ._i_miso_unsync(pmod1[0])
        // , .__output({ld5, ld6, ld7, ld8})
        , .__output({
            hsync,
            vsync,
            r,
            g,
            b,
            ld5,
            ld6,
            ld7,
            ld8
        })
        );
endmodule
